import mutations from "./mutationsTSR";
import actions from "./actionsTSR";
import getters from "./gettersTSR";
import state from "./stateTSR";

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
};
