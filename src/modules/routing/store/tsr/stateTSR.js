import tsrWaypointsSource from "../../js/map/tsr/waypoints/tsrWaypointsSource";
import tsrWaypointsLayer from "../../js/map/tsr/waypoints/tsrWaypointsLayer";
import tsrWaypointsModifyInteraction from "../../js/map/tsr/waypoints/tsrWaypointsModify";
import tsrWaypointsSnapInteraction from "../../js/map/tsr/waypoints/tsrWaypointsSnap";
import tsrWaypointsDrawInteraction from "../../js/map/tsr/waypoints/tsrWaypointsDraw";

import tsrRouteSource from "../../js/map/tsr/route/tsrRouteSource";
import tsrRouteLayer from "../../js/map/tsr/route/tsrRouteLayer";

import tsrElevationSource from "../../js/map/tsr/elevation/tsrElevationSource";
import tsrElevationLayer from "../../js/map/tsr/elevation/tsrElevationLayer";

import stateRouting from "../stateRouting";

export default {
    // Map State
    tsrWaypointsSource,
    tsrWaypointsLayer,

    tsrRouteSource,
    tsrRouteLayer,

    tsrElevationSource,
    tsrElevationLayer,

    // Draw Parameters
    tsrWaypointsModifyInteraction,
    tsrWaypointsSnapInteraction,
    tsrWaypointsDrawInteraction,

    // tsr Parameters
    waypoints: [],
    // addStartEndPoint defines whether a waypoint,
    // startpoint or endpoint is to be added.
    // 0 - startpoint
    // 1 - waypoint
    // 2 - endpoint
    addStartEndPoint: 1,
    // Routing tsr Result
    tsrDirections: null,
    tsrDistance: "",
    tsrDuration: "",
    mapListenerAdded: false,
    isLoadingDirections: false,
    mapInteractionMode: "WAYPOINTS",
    settings: stateRouting.tsrSettings
};
