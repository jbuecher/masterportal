import {Modify} from "ol/interaction.js";
import tsrWaypointsSource from "./tsrWaypointsSource";

export default new Modify({source: tsrWaypointsSource});
