import {Snap} from "ol/interaction.js";
import tsrWaypointsSource from "./tsrWaypointsSource";

export default new Snap({source: tsrWaypointsSource});
